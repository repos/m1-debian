#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

cd "$(dirname "$0")"

unset LC_CTYPE
unset LANG

mkdir -p build
cd build

export META_VERSION=6.13.5-2-cy8aer0
export LINUX_IMAGE_VERSION=6.13.5-asahi-2-cy8aer0

rm -rf linux-image-asahi_${META_VERSION}_arm64
mkdir -p linux-image-asahi_${META_VERSION}_arm64/DEBIAN
cat > linux-image-asahi_${META_VERSION}_arm64/DEBIAN/control <<EOF
Package: linux-image-asahi
Version: $META_VERSION
Section: base
Depends: m1n1,u-boot-asahi (>= 2024.01.23-6+cy8aer1),asahi-scripts,asahi-fwextract,asahi-audio,linux-image-${LINUX_IMAGE_VERSION}
Conflicts: linux-image-asahi-headless
Replaces: linux-image-asahi-headless
Architecture: arm64
Maintainer: Thomas Renard <thomas.renard@g3la.de>
Description: Linux for 64-bit apple silicon machines (meta-package)
EOF
dpkg-deb --build linux-image-asahi_${META_VERSION}_arm64

rm -rf linux-image-asahi-headless_${META_VERSION}_arm64
mkdir -p linux-image-asahi-headless_${META_VERSION}_arm64/DEBIAN
cat > linux-image-asahi-headless_${META_VERSION}_arm64/DEBIAN/control <<EOF
Package: linux-image-asahi-headless
Version: $META_VERSION
Section: base
Depends: m1n1,u-boot-asahi (>= 2024.01.23-6+cy8aer1),asahi-scripts,asahi-fwextract,linux-image-${LINUX_IMAGE_VERSION}
Recommends: asahi-audio
Conflicts: linux-image-asahi
Replaces: linux-image-asahi
Architecture: arm64
Maintainer: Thomas Renard <thomas.renard@g3la.de>
Description: Linux for 64-bit apple silicon machines (meta-package).
 Headless version without audio dependencies.
EOF
dpkg-deb --build linux-image-asahi-headless_${META_VERSION}_arm64
