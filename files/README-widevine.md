# Widevine for asahi linux

To manually re-install the widevine plugin call

```
/usr/lib/widevine-installer/widevine-installer
``` 

Then restart the desktop session or rebooting is needed.

To get the plugin working a ChromeOS aarch64 user-agent has to be set. 
The user's login shell has to be bash.

It is recommended to use the plugins:

- User Agent Switcher and Manager
  (https://addons.mozilla.org/firefox/addon/user-agent-string-switcher/)
- Firefox Multi-Account Containers

Then

- Create a Container for all widevine-needed Sites and put these into
  it.
- Open a site in the container
- In Firefox Multi-Account Containers select a ChromeOS user-agent for
  aarch64
- Press _Apply (container)_

Now the content of the site could be played.
