#!/usr/bin/env bash

# SPDX-License-Identifier: MIT

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

cd "$(dirname "$0")"

# export CARGO_HOME="$(pwd)/build/cargo"
# export RUSTUP_HOME="$(pwd)/build/rust"
# source "$(pwd)/build/cargo/env"

export PATH=/usr/local/bin:/usr/local/bin:/usr/bin:/usr/bin:/sbin:/bin

export CLANG_VERSION=-19

unset LC_CTYPE
unset LANG

export KERNEL_VERSION=asahi-6.13.5-2
export KERNEL_LOCALVERSION=-2-cy8aer0
export M1N1_VERSION=1.4.14-cy8aer0
export M1N1_GIT=v1.4.14

handle_crosscompile()
{
    if [ "`uname -m`" != 'aarch64' ]; then
        export ARCH=arm64
        export CROSS_COMPILE=aarch64-linux-gnu-
        sudo apt install -y libc6-dev-arm64-cross
    fi
}

build_linux()
{
    (
	handle_crosscompile
	test -d linux || git clone https://github.com/AsahiLinux/linux
	cd linux
	git fetch -a -t
	git fetch origin speakers/enablement-READ-COMMIT-MESSAGE
	git reset --hard $KERNEL_VERSION;
	cat ../../config.txt > .config
	make LLVM=${CLANG_VERSION} rustavailable
	make LLVM=${CLANG_VERSION} olddefconfig
	cp .config ~/tmp/config
	make -j `nproc` LLVM=${CLANG_VERSION} V=0 bindeb-pkg LOCALVERSION=$KERNEL_LOCALVERSION > /dev/null
    )
}

build_m1n1()
{
    (
        test -d m1n1 || git clone --recursive https://github.com/AsahiLinux/m1n1
        cd m1n1
        git fetch -a -t
        git reset --hard $(M1N1_GIT);
        make -j `nproc`
    )
}

build_uboot()
{
    (
        handle_crosscompile
        test -d u-boot || git clone https://github.com/AsahiLinux/u-boot
        cd u-boot
        git fetch -a -t
        git reset --hard asahi-v2024.10-1;  # asahi branch from 8. Jan. 2024, 19:51 MEZ
        patch -p1 <../../files/u-boot-bootmenu.patch
	make clean
        make apple_m1_defconfig
        make -j `nproc`
    )
    cat m1n1/build/m1n1.bin   `find linux/arch/arm64/boot/dts/apple/ -name \*.dtb` <(gzip -c u-boot/u-boot-nodtb.bin) > u-boot.bin
}

package_u-boot-asahi()
{
    {
	export U_BOOT_VERSION=2024.10-1+cy8aer1
	rm -rf u-boot-asahi_${U_BOOT_VERSION}_arm64
	mkdir -p u-boot-asahi_${U_BOOT_VERSION}_arm64/usr/lib/u-boot-asahi/
	mkdir -p u-boot-asahi_${U_BOOT_VERSION}_arm64/DEBIAN
	cp u-boot/u-boot.bin u-boot-asahi_${U_BOOT_VERSION}_arm64/usr/lib/u-boot-asahi/u-boot.bin
	cp u-boot/u-boot-nodtb.bin u-boot-asahi_${U_BOOT_VERSION}_arm64/usr/lib/u-boot-asahi/u-boot-nodtb.bin
	cat <<EOF > u-boot-asahi_${U_BOOT_VERSION}_arm64/DEBIAN/control
Package:     u-boot-asahi
Version: $U_BOOT_VERSION
Section: base
Depends: asahi-scripts (>= 20230821-3)
Priority: optional
Architecture: arm64
Maintainer: Thomas Renard <thomas.renard@g3la.de>
Description: U-Boot boot loader Apple silicon
 This is the asahi version of u-boot with asahi patches.
EOF

	cat > u-boot-asahi_${U_BOOT_VERSION}_arm64/DEBIAN/postinst <<'EOF'
#!/bin/bash

export PATH=/bin
if [ -f /boot/efi/m1n1/boot.bin ]; then
        cp /boot/efi/m1n1/boot.bin /boot/efi/m1n1/`date +%Y%m%d%H%M`.bin
fi
/usr/bin/update-m1n1
EOF

        chmod 755 u-boot-asahi_${U_BOOT_VERSION}_arm64/DEBIAN/postinst


	dpkg-deb --build u-boot-asahi_${U_BOOT_VERSION}_arm64
    }
}

package_m1n1()
{
    (
        rm -rf m1n1_${M1N1_VERSION}_arm64
        mkdir -p m1n1_${M1N1_VERSION}_arm64/DEBIAN m1n1_${M1N1_VERSION}_arm64/usr/lib/m1n1/
        cp u-boot.bin m1n1_${M1N1_VERSION}_arm64/usr/lib/m1n1/boot.bin
        cp m1n1/build/m1n1.bin m1n1_${M1N1_VERSION}_arm64/usr/lib/m1n1/m1n1.bin
        cat <<EOF > m1n1_${M1N1_VERSION}_arm64/DEBIAN/control
Package: m1n1
Version: $M1N1_VERSION
Section: base
Depends: asahi-scripts (>= 20231219.1-1)
Priority: optional
Architecture: arm64
Maintainer: Thomas Renard <thomas.renard@g3la.de>
Description: Apple silicon boot loader
 Next to m1n1 this also contains the device trees and u-boot.
EOF


        cat > m1n1_${M1N1_VERSION}_arm64/DEBIAN/postinst <<'EOF'
#!/bin/bash

export PATH=/bin
if [ -f /boot/efi/m1n1/boot.bin ]; then
        cp /boot/efi/m1n1/boot.bin /boot/efi/m1n1/`date +%Y%m%d%H%M`.bin
fi
/usr/bin/update-m1n1
EOF

        chmod 755 m1n1_${M1N1_VERSION}_arm64/DEBIAN/postinst

	dpkg-deb --build m1n1_${M1N1_VERSION}_arm64
    )
}


mkdir -p build
cd build

build_linux
build_m1n1
build_uboot
# package_m1n1
package_u-boot-asahi
