#!/bin/bash

sudo apt-get install -y build-essential bash git locales gcc-aarch64-linux-gnu \
     libc6-dev device-tree-compiler imagemagick ccache eatmydata debootstrap \
     pigz libncurses-dev qemu-user-static binfmt-support rsync git flex bison bc \
     kmod cpio libncurses5-dev libelf-dev:native libssl-dev dwarves zstd lsb-release \
     clang-19 llvm-19 libllvmspirvlib-19-dev clang-19 rust-src librust-bindgen-dev \
     rustfmt bindgen debhelper-compat uuid-dev libgnutls28-dev
