#!/usr/bin/env bash

echo "This is now done as a debian package!"
echo "See https://git.g3la.de/repos/widevine-installer"

exit 0

export WIDEVINE_INSTALLER_VERSION=20240504-cy8aer3

build_widevine_installer()
{
    test -d widevine-installer || git clone https://github.com/AsahiLinux/widevine-installer.git
    pushd widevine-installer
    cat ../../files/gmpwidevine.patch | patch -p1
    popd
    mkdir -p widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64
    mkdir -p widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/usr/lib/widevine-installer
    mkdir -p widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/usr/share/doc/widevine-installer
    mkdir -p widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/DEBIAN
    
    cp -a widevine-installer/* widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/usr/lib/widevine-installer
    sed -i "s/\/bin\/sh/\/bin\/bash/g" widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/usr/lib/widevine-installer/widevine-installer
    cp ../files/README-widevine.md widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/usr/share/doc/widevine-installer/README.md

    cat <<EOF > widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/DEBIAN/control
Package: widevine-installer
Version: $WIDEVINE_INSTALLER_VERSION
Section: base
Depends: squashfs-tools
Priority: optional
Architecture: arm64
Maintainer: Thomas Renard <thomas.renard@g3la.de>
Description: Widevine Installer
 Scripts to install libwidevine for Firefox and Chromium
EOF

    cat > widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/DEBIAN/postinst <<'EOF'
#!/bin/bash

export PATH=/bin
if [ -f /usr/lib/widevine-installer/widevine-installer ]; then
   /usr/lib/widevine-installer/widevine-installer

   echo
   echo "Please read /usr/share/doc/widevine-installer/README.md"
fi
EOF

    chmod 755 widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64/DEBIAN/postinst
    
    dpkg-deb --build widevine-installer_${WIDEVINE_INSTALLER_VERSION}_arm64

}

mkdir -p build
cd build

build_widevine_installer


    

   
